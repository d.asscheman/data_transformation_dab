# DAB Data transform script

This script is used to transform the export data from DAB.uu.nl.
The output file from this script will have 1 record for each participant/task.
## The columns are:
- project_name
- participant_case_number 
    - Unique case number which refers to database of reseacher.
- oganisation_name
- group_name    
    - group to which the participant belongs to.
- id
    - participant id, this is a DAB internal reference id.
- task_id 
    - this is a DAB internal reference id.
- #levels
    - number of levels for this task
- #items
    - number of items for this task
- language
- theme
- matrix_size   
    - size of the matrix displayed to the participant.
- date          
    - datetime of the first save, in general the save of the introduction
- L*_I*_Q*      
    - score of this level-item-question (0 = incorrect, 1 = correct)
- L*_I*_Q*_RT   
    - response time of this level-item-question in millisecond. Response time is started after the 
    question is asked and the participant is allowed to click.
- L*_I*_prop    
    - proportion score for this level-item. This is defined as number question correct/number of questions asked.
- meanprop (In development)    
    - proportion score for this task. This is defined as number question correct/number of questions asked.
- totalquestions
    - total number of questions a participant anwsered for a task, can be used to check if a participant finished all questions
- totalitems
    - total number of items a participant anwsered for a task, can be used to check if a participant finished all items

## how to use:
- Make sure you have R running on your computer: https://cran.r-project.org/
- Download this repository by clicking on ‘download’ at the top right of this page 
- Save the download on your computer (when you download a zip folder, make sure to extract the folders and files)
- Download your results in .csv  format from UU-DAB site and copy the unchanged file to the 'source_data' folder.
- Run one of the scripts (depending on the operating system):
  - Linux: In a terminal, go to the folder where the file DabConvert.R is and run 'Rscript DabConvert.R'
  - Windows: 
    - Open R. 
    - Click menu item 'file' -> 'Change dir...'
    - Select the folder where the the file DabConvert.R is located
    - In the R console type and hit enter: source('DabConvert.R')
- For each file in the source_data folder a transformed file is created in the processed_data folder.
The files have the same name + a timestamp in the name. So the script will never overwrite files in the processed_data folder.